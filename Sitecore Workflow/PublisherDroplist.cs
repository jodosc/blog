﻿using Sitecore.Diagnostics;
using Sitecore.Security.Accounts;
using Sitecore.Web.UI.HtmlControls;
using System;
using GlobalSitecore = Sitecore;

namespace DemoSite.Foundation.SitecoreExtensions.Controls
{
    public class PublisherDroplist : Control
    {
        public PublisherDroplist()
        {
            Class = "scContentControl";
            Activation = true;
        }

        public bool HasPostData { get; set; }

        protected override void DoRender(System.Web.UI.HtmlTextWriter output)
        {
            string error = null;
            output.Write("<select" + GetControlAttributes() + ">");
            output.Write("<option value = \"\"></option>");

            Role role = GetRoleByName("sitecore\\DemoSite Publisher");
            if(role != null)
            {
                var usersInRole = RolesInRolesManager.GetUsersInRole(role, true);
                foreach (var user in usersInRole)
                {
                    var displayName = !string.IsNullOrEmpty(user.DisplayName) ? user.DisplayName : user.Name;
                    output.Write(String.Format(@"<option value=""{1}"" {2}>{0}</option>", displayName, user.Name, Value == user.Name ? "selected=\"selected\"" : String.Empty));
                }
            }
            else
            {
                error = $"Could not found the sitecore role \"sitecore\\DemoSite Publisher\"";
            }            

            if (error != null)
            {
                output.Write("<optgroup label=\"" + error + "\">");
                output.Write("<option value=\"" + Value + "\" selected=\"selected\">" + Value + "</option>");
                output.Write("</optgroup>");
            }
            output.Write("");

            if (error != null)
            {
                output.Write("{0}", error);
            }
        }

        protected override bool LoadPostData(string value)
        {
            HasPostData = true;

            if (value == null)
            {
                return false;
            }

            Log.Info(this + " : Field : " + GetViewStateString("Field"), this);
            Log.Info(this + " : FieldName : " + GetViewStateString("FieldName"), this);

            if (GetViewStateString("Value") != value)
            {
                SetModified();
            }

            SetViewStateString("Value", value);
            return true;
        }

        protected override void OnLoad(EventArgs e)
        {
            Assert.ArgumentNotNull(e, "e");
            base.OnLoad(e);

            if (!HasPostData)
            {
                LoadPostData(string.Empty);
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            Assert.ArgumentNotNull(e, "e");
            base.OnPreRender(e);
            ServerProperties["Value"] = ServerProperties["Value"];
        }

        private static void SetModified()
        {
            GlobalSitecore.Context.ClientPage.Modified = true;
        }

        private Role GetRoleByName(string name)
        {
            try
            {
                return Role.FromName(name);
            }
            catch (Exception ex)
            {
                Log.Error("[WORKFLOW - CUSTOM CONTROL PUBLISHER]", ex, this);
            }
            return null;
        }
    }
}