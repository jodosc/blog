<%@ Application Language='C#' Inherits="Sitecore.Web.Application" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Web" %>
<script runat="server">
    private const string credentials = "[yourusername]:[yourpassword]";
    private string[] allow = new string[] { "/404", "/500", "/api/" };

    void Application_BeginRequest(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(credentials)) return;

        var application = sender as HttpApplication;
        if (application == null) return;

        var context = application.Context as HttpContext;
        if (context == null) return;

        var request = context.Request as HttpRequest;
        if (request == null) return;

        var response = context.Response as HttpResponse;
        if (response == null) return;

        if (allow.Any(o => request.Url.AbsolutePath.StartsWith(o, StringComparison.OrdinalIgnoreCase))) return;

        var authorization = request.Headers["Authorization"];
        if (String.IsNullOrWhiteSpace(authorization) || !IsValid(authorization))
        {
            response.StatusCode = 401;
            response.AddHeader("WWW-Authenticate", "Basic realm=\"" + (request.Url.Host.ToLower()) + "\"");
            response.Write("Access Denied");
            response.Flush();
            response.Close();
            application.CompleteRequest();
        }
    }

    private bool IsValid(string authorization)
    {
        try
        {
            var parts = authorization.Split(new char[] { ' ' }, 2);
            if (parts.Length != 2 || !String.Equals(parts[0], "Basic", StringComparison.OrdinalIgnoreCase) || String.IsNullOrWhiteSpace(parts[1])) return false;
            Encoding encoding = Encoding.GetEncoding("iso-8859-1");
            var decoded = encoding.GetString(Convert.FromBase64String(parts[1]));
            if (String.Equals(decoded, credentials, StringComparison.OrdinalIgnoreCase)) return true;

        }
        catch (ArgumentException) { /* Authentication Failed */ }
        catch (FormatException) { /* Authentication Failed */ }
        return false;
    }
</script>
<%-- WARNING: Every custom application must derive from the Sitecore.Web.Application class. Otherwise some Sitecore features will not be available or will not work correctly. --%> 
